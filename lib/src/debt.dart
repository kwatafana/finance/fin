import './fin.dart';
import './errors.dart';

/// ⚖️ In the level method, both interest and principal proportions remain the
/// same throughout the entire maturity, and as a result, their relationship
/// to each other remains consistent for the entire period.
class LevelMethod extends Fin {
  LevelMethod({num? principal, num? interest, num? term})
      : super(principal: principal, interest: interest, term: term);

  /// Calculate the monthly portions to be paid for interest, principal
  Map<String, num> monthlyPortions(num numberOfPayments) {
    checkPrincipal();
    checkInterest();
    checkTerm();

    num i = principal! * (interest! / 100) * term!;
    num pyt = principal! + i;
    pyt = pyt / numberOfPayments;
    final num mip = i / numberOfPayments;
    final num mpp = pyt - mip;
    return {
      "monthlyPayments": pyt,
      "monthlyInterestPortion": mip,
      "monthlyPrincipalPortion": mpp
    };
  }
}

/// ↕️ Under this method, interest and principal portions are in a reverse
/// relationship. While the interest portion decreases, the principal portion
/// increases throughout the maturity period. Interest for each payment is
/// determined by a certain fraction calculated according to the rule of 78.
/// This rule obtained its name based on the total number of monthly payments
/// within a year (1 + 2 + 3 + · · · + 12 =78). The number (78) serves as a
/// denominator of a fraction used to determine the interest portion for any
/// month. This number would also change based on the maturity term for a
/// total number of payments. For example, 24 payments would total
/// $300 (1 + 2 + 3 + · · · + 24 = $300), 36 payments would total $666
/// (1 + 2 + 3 + · · · + 36 = $666), and so on.
///
/// This function calculates the interest portion of an amortized debt for a
/// specific payment (nthpayment).
class RuleOf78Method extends Fin {
  RuleOf78Method({principal, interest, term})
      : super(principal: principal, interest: interest, term: term);

  /// Calculate the monthly portion to be paid for interest
  num ruleOf78MethodInterestPortion(num numberOfPayments, num nthPayment) {
    num ti = totalInterest();
    num mip = ruleOf78Numerator(numberOfPayments, nthPayment) /
        ruleOf78Denominator(numberOfPayments);
    mip = mip * ti;
    return mip;
  }

  /// Calculate the denominator for use in calculating the interest by using
  /// the rule of 78 method
  num ruleOf78Denominator(num numberOfPayments) {
    num d = numberOfPayments + 1;
    d = d * numberOfPayments;
    d = d / 2;
    return d;
  }

  /// Calculate the numerator for use in calculating the interest by using
  /// the rule of 78 method
  num ruleOf78Numerator(num numberOfPayments, num nthPayment) {
    if (nthPayment > numberOfPayments) {
      FinError.invalidInput;
    }

    num? j;
    for (var i = 0; i < numberOfPayments; i++) {
      if (i == nthPayment) {
        j = numberOfPayments - (i - 1);
        break;
      }
    }

    return j!;
  }
}
