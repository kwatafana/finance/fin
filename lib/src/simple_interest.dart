import './fin.dart';

/// 🔹 In the simple interest method, interest is assessed on the principal only.
class SimpleInterest extends Fin {
  SimpleInterest({num? principal, num? interest, num? term})
      : super(principal: principal, interest: interest, term: term);

  /// Obtain the rate of interest if total interest earned, term, and
  /// principal are all given
  num calInterest(num totalInterestEarned) {
    checkPrincipal();
    checkTerm();

    num i = totalInterestEarned / (principal! * term!);
    return i * 100.0;
  }

  /// we can obtain the term of maturity given principal, total simple interest
  /// earnings, and interest
  num calTerm(num totalInterestEarned) {
    checkPrincipal();
    checkInterest();
    final i = interest! / 100;
    return totalInterestEarned / (principal! * i);
  }

  /// We can obtain CV given total earnings , interest, and term.
  num calPrincipal(num totalInterestEarned) {
    checkInterest();
    checkTerm();

    final i = interest! / 100;
    return totalInterestEarned / (i * term!);
  }

  /// If we add the total interest earned to the principal, we obtain the
  /// future value (FV), to which the current value will grow.
  num futureValue() {
    checkPrincipal();
    checkInterest();
    checkTerm();

    final i = interest! / 100.0;
    var t = 1 + (term! * i);
    t = t * principal!;
    return t;
  }

  /// This process of getting the current value is called the simple discount,
  /// for it brings the future value back from its maturity date to the current
  /// time.
  /// We can use this simple discount to get the current value of a future
  /// value in the two types of debt: non-interest- and the interest-bearing
  /// debt.
  num simpleDiscount(num futureValue) {
    checkInterest();
    checkTerm();

    num sd = interest! / 100.0;
    sd = sd * term!;
    sd = sd + 1;
    sd = futureValue / sd;
    return sd;
  }
}
