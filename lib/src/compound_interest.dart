import 'dart:math';
import './fin.dart';

/// 💠 In the compound interest method, interest is earned on
/// the principal as well as on any interest earned. That is why compound
/// interest is called “interest on interest.”
class CompoundInterest extends Fin {
  CompoundInterest({num? principal, num? interest, num? term})
      : super(principal: principal, interest: interest, term: term);

  /// The future value, to which the current value will grow.
  num futureVal() {
    checkPrincipal();
    checkInterest();
    checkTerm();

    num i = interest! / 100;
    num fv = 1 + i;
    fv = pow(fv, term!);
    return principal! * fv;
  }

  /// The future value in the compounding formula can be discounted into a
  /// current value
  num currentVal(num futureVal) {
    checkInterest();
    checkTerm();

    num i = interest! / 100;
    num cv = 1 + i;
    cv = pow(cv, -term!);
    return futureVal * cv;
  }

  // TODO: The compound interest rate can be found in terms of all other
  // remaining factors in the compounding formula,
  //num calRate(num futureVal) {
  //}

  // TODO: The term (n) can also be obtained in relation to all other
  // compounding factors
  //num calTerm(num futureVal) {}

  //TODO: rule of 72

  // TODO: Effective interest rate

  // TODO: continuos compounding
}
