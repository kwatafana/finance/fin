/// 🚨 Fin errors
class FinError {
  /// Principal value is null
  static Exception undefinedPrincipal =
      throw Exception('Principal value is null');

  /// Interest value is null
  static Exception undefinedInterest =
      throw Exception('Interest value is null');

  /// Term value is null
  static Exception undefinedTerm = throw Exception('Term value is null');

  /// Invalid input
  static Exception invalidInput = throw Exception('Invalid Input');
}
