import 'package:fin/fin.dart';
import 'package:test/test.dart';

void main() {
  group('Tests for LevelMethod', () {
    test('Test monthlyPortions', () {
      final debt = LevelMethod(principal: 1600, interest: 12, term: 1);
      final monthly = debt.monthlyPortions(12);

      expect(monthly['monthlyPayments']!.round(), 149);
      expect(monthly['monthlyInterestPortion'], 16);
      expect(monthly['monthlyPrincipalPortion']!.round(), 133);
      expect(
          monthly['monthlyInterestPortion']! +
              monthly['monthlyPrincipalPortion']!.round(),
          149);
    });
  });

  group('Tests for RuleOf78Method', () {
    test('Test ruleOf78Denominator', () {
      final debt = RuleOf78Method();
      final denominator = debt.ruleOf78Denominator(60);
      expect(denominator, 1830);
    });

    test('Test ruleOf78Numerator', () {
      final debt = RuleOf78Method();
      final denominator = debt.ruleOf78Numerator(24, 4);
      final denominator2 = debt.ruleOf78Numerator(24, 12);
      expect(denominator, 21);
      expect(denominator2, 13);
    });

    test('Test ruleOf78MethodInterestPortion', () {
      final amortizedDebt =
          RuleOf78Method(principal: 1600, interest: 12, term: 1);
      final monthly =
          amortizedDebt.ruleOf78MethodInterestPortion(12, 6).round();
      expect(monthly, 17);
    });
  });
}
