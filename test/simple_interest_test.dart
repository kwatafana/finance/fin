import 'package:fin/fin.dart';
import 'package:test/test.dart';

void main() {
  group('Tests for SimpleInterest', () {
    test('Test simple interest total earnings', () {
      // What would the total interest be on a deposit of $2,000 for 5 years
      // in a savings account yielding 0.035% annual simple interest?
      final simpleInterest =
          SimpleInterest(principal: 2000, interest: 0.035 * 100, term: 5);
      final totalEarnings = simpleInterest.totalInterest();
      expect(totalEarnings, 350);
    });

    test('Test simple interest, interest rate', () {
      // Jill saved $1,500 in her local bank for 3 years and earned
      // $236.25. What would the rate of interest have been?
      final simpleInterest = SimpleInterest(principal: 1500, term: 3);
      final interest = simpleInterest.calInterest(236.25);
      expect(interest, 5.25);
    });

    test('Test simple interest maturity', () {
      // Suppose that another local bank offers Jill 0.055% interest if she
      // doubles her deposit with them, and that she can earn a total of $825
      // in interest after a certain time. How long would Jill have to keep
      // her money in that bank to earn the $825 interest?
      final simpleInterest =
          SimpleInterest(principal: 3000, interest: 0.055 * 100);
      final term = simpleInterest.calTerm(825);
      expect(term, 5);
    });

    test('Test simple interest principal calculation', () {
      // How much should Jill deposit at 6% interest to collect $1,000 in
      // total interest at the end of 4 years?
      final simpleInterest = SimpleInterest(interest: 6, term: 4);
      final principal = simpleInterest.calPrincipal(1000).round();
      expect(principal, 4167);
    });

    test('Test simple future value', () {
      final simpleInterest =
          SimpleInterest(principal: 1000, interest: 2, term: 4);
      final future_value = simpleInterest.futureValue();
      expect(future_value, 1080);

      // Amy borrowed $900 for 16 months at an annual simple interest of 7%.
      // How much would the payoff be?
      final simpleInterest2 =
          SimpleInterest(principal: 900, interest: 7, term: 1.33);
      final future_value2 = simpleInterest2.futureValue();
      expect(future_value2, 983.79);
    });

    /// Determine the simple discount of a debt of $5,500 that is due in 8
    /// months when the interest rate is 6.5%. FV = $5,500; n = 8 months;
    /// r = 6.5%
    test('Test simple discount', () {
      final simpleInterest = SimpleInterest(interest: 6.5, term: 8 / 12);
      final simpleDiscount = simpleInterest.simpleDiscount(5500).round();
      expect(simpleDiscount, 5272);
    });
  });
}
