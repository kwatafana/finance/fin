/// # 💸 fin
///
/// Dart library for mathematical finance.
///
/// ## 📉 Time value of money
///
/// Generally in the absence of an interest rate, money tends to have
/// a __higher__ value in the present and a __lower__ value in the
/// future. Three factors may explain this fact:
/// - __Inflation__: a steady rise in the general level of the price
///   of goods and services.
/// - __Consumer Impatience__: refers to people's general preference
///   for today's satisfaction over tomorrow's.
/// - __Life Uncertainty__: risks to the extension of time to utilize money.
///
/// ## 💸 Interest
/// If someone has to wait and forgo the immediate satisfaction
/// brought about by spending the money, a reward to compensate for
/// the sacriﬁce should be due. We call this reward __interest__, defined
/// as the price of money services.
///
/// Interest is the focal point of the time value of money concept
/// and the theoretical and practical core of ﬁnance.
///
/// The rate of interest is the reward that would have to be paid by
/// a borrower to a lender for the use of money borrowed. This rate
/// is usually expressed as a percentage of the original amount of
/// money borrowed. Since money is generally
library;

export 'src/errors.dart';
export 'src/simple_interest.dart';
export 'src/compound_interest.dart';
export 'src/debt.dart';
