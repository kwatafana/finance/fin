import 'package:fin/fin.dart';
import 'package:test/test.dart';

void main() {
  group('Tests for CompoundInterest', () {
    test('Test future value', () {
      // calculating 10% compound interest on $1000 for 3 years
      final compoundInterest =
          CompoundInterest(principal: 1000, interest: 10, term: 3);
      final futureVal = compoundInterest.futureVal().round();
      expect(futureVal, 1331);

      // Heather invested $12,000 for 5 years in an account earning 0.0733%
      // annual compound interest. How much would she expect to collect at the
      // end of 5 years?
      final compoundInterest2 =
          CompoundInterest(principal: 12000, interest: 0.0733 * 100, term: 5);
      final futureVal2 = compoundInterest2.futureVal().round();

      expect(futureVal2, 17092);

      // calculate the future value of an investment amount of $930 for 6 years
      // if the annual compound interest rate is 7 12 %
      final compoundInterest3 =
          CompoundInterest(principal: 930, interest: 0.075 * 100, term: 6);
      final futureVal3 = compoundInterest3.futureVal().round();
      expect(futureVal3, 1435);
    });

    test('Test current value', () {
      // An amount of $5,000 is to be inherited in 4 years. How much would it
      // be if it is cashed in now given that the interest rate is 6 34 %
      // compounded annually?
      final compoundInterest =
          CompoundInterest(interest: 0.0675 * 100, term: 4);
      final futureVal = compoundInterest.currentVal(5000).round();
      expect(futureVal, 3850);
    });
  });
}
