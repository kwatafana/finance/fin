import './errors.dart';

/// Financial Object
abstract class Fin {
  Fin({this.principal, this.interest, this.term});

  /// The worth of money in the current period, as it is represented by the
  /// principal saved or invested.
  num? principal;

  /// The prevailing interest rate, which is the rate that is applied to the
  /// current value to turn it into a future value. (%)
  num? interest;

  /// The time of maturity, the time span between the current and future values (years)
  num? term;

  /// Calculate the total interest to be paid or earned on the finalcial object
  num totalInterest() {
    checkPrincipal();
    checkInterest();
    checkTerm();

    return principal! * (interest! / 100) * term!;
  }

  void checkPrincipal() {
    if (principal == null) {
      FinError.undefinedPrincipal;
    }
  }

  void checkInterest() {
    if (interest == null) {
      FinError.undefinedInterest;
    }
  }

  void checkTerm() {
    if (term == null) {
      FinError.undefinedTerm;
    }
  }
}
